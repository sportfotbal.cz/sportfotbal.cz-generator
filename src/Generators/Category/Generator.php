<?php
namespace Andering\Generator\Generators\Category;
use Andering\Generator\Generators\BaseGenerator;

class Generator extends BaseGenerator {

    protected function getTemplate($name)
    {
        $reflection = new \ReflectionClass(__CLASS__);
        return dirname($reflection->getFileName()) . '/latte/' . $name . '.latte';
    }

}
