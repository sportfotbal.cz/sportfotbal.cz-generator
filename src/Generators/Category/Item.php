<?php

namespace Andering\Generator\Generators\Category;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;

class Item extends BaseItem {

    protected $id;
    protected $title;
    protected $description;
    protected $link;
    protected $hierarchy;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function getHierarchy()
    {
        return $this->hierarchy;
    }

    public function setHierarchy($hierarchy)
    {
        $this->hierarchy = $hierarchy;
        return $this;
    }


}
