<?php

namespace Andering\Generator\Generators\Heureka;

use Andering, Nette;

/**
 * Class Gift
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Heureka
 */
class Gift extends Nette\Object {

    /** @var string */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gift constructor.
     * @param $name
     */
    public function __construct($name)
    {

        $this->name = (string)$name;
    }

}
