<?php
namespace Andering\Generator\Generators;

use Latte\Engine;
use Andering\Generator\IStorage;
use Nette\Object;
use Andering\Generator\FileEmptyException;
use Andering\Generator\ItemIncompletedException;

/**
 * Class BaseGenerator
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators
 */
abstract class BaseGenerator extends Object implements IGenerator {

    /** @var resource|bool|null temp file */
    private $handle;

    private $header;

    private $footer;

    private $xml = [];

    /** @var \Andering\Generator\Storage */
    private $storage;

    /** @var array */
    public $onGenerate;

    /**
     * BaseGenerator constructor.
     * @param \Anderng\Generator\Storage $storage
     */
    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * @param $name
     * @return string path to template
     */
    abstract protected function getTemplate($name);


    public function setHeader($header)
    {
        $this->header = (object)$header;
    }

    public function setFooter($footer)
    {
        $this->footer = (object)$footer;
    }


    public function addHeader() {
        $this->xml[] = $this->prepareTemplate('header',['header' => $this->header]);
    }

    public function addItem(IItem $item)
    {


        if (!$item->validate()) {
            throw new ItemIncompletedException('Item is not complete');
        }

        $this->xml[] = $this->prepareTemplate('item',['item' => $item]);
    }

    public function addFooter() {
        $this->xml[] = $this->prepareTemplate('footer',['footer' => $this->footer]);
    }

    /**
     * @param $filename
     * @return void
     */
    public function save($filename)
    {

        $this->addHeader();
        $this->onGenerate($this);
        $this->addFooter();


        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = implode("",$this->xml);

        $dom->loadXML($xml);

        $this->storage->save($filename,$dom->saveXML());

    }

    /**
     * @param $filename
     * @return void
     */
    public function output()
    {

        $this->addHeader();
        $this->onGenerate($this);
        $this->addFooter();


        $dom = new \DOMDocument('1.0');
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = implode("",$this->xml);

        $dom->loadXML($xml);

        echo $dom->saveXML();

    }

    /**
     * @param $template
     */
    protected function prepareTemplate($template,$data = [])
    {
        $latte = new Engine;
        return $latte->renderToString($this->getTemplate($template),$data);
    }

}
