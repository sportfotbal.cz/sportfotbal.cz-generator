<?php

namespace Andering\Generator\Generators\Formuleshop;

use Andering, Nette;

/**
 * Class Parameter
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Heureka
 */
class Variant extends Nette\Object {


    protected $size;
    protected $color;
    protected $ean;
    protected $productno;
    protected $delivery_date;
    /**
     * Parameter constructor.
     * @param $name
     * @param $value
     */
    public function __construct($size, $color, $ean, $productno, $delivery_date)
    {
        $this->size = $size;
        $this->color = $color;
        $this->ean = $ean;
        $this->productno = $productno;
        $this->delivery_date = $delivery_date;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getEan()
    {
        return $this->ean;
    }

    public function getProductno()
    {
        return $this->productno;
    }

    public function getDeliveryDate()
    {
        return $this->delivery_date;
    }

}
