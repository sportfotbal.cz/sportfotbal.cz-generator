<?php
namespace Andering\Generator\Generators;

/**
 * Interface IGenerator
 * @package Andering\Generator\Generators
 */
interface IGenerator {



    /**
     * Save file
     * @param $filename
     * @return mixed
     */
    public function save($filename);
}
