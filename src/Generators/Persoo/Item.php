<?php

namespace Andering\Generator\Generators\Persoo;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;

class Item extends BaseItem {

    protected $id;
    protected $title;
    protected $description;
    protected $link;
    protected $images = [];
    protected $categories = [];
    protected $parameters = [];
    protected $labels = [];
    protected $originalPriceVat;
    protected $finalPriceVat;
    protected $gtin;
    protected $mpn;
    protected $sku;
    protected $brand;
    protected $currency;
    protected $itemGroupId;
    protected $availability;


    public function addImage($url)
    {
        $this->images[] = (object) ['url' => $url];
        return $this;
    }

    public function getImages()
    {
        return $this->images;
    }

    public function addCategory($hierarchy, $category)
    {
        $this->categories[] = (object) ['category' => $category, 'hierarchy' => $hierarchy];
        return $this;
    }

    public function getCategories()
    {
        return $this->categories;
    }

    public function addParameter($name,$value)
    {
        $this->parameters[] = (object) ['name' => $name, 'value' => $value];
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function addLabel($label)
    {
        $this->labels[] = (object) ['name' => $label];
        return $this;
    }

    public function getLabels()
    {
        return $this->labels;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setLink($link)
    {
        $this->link = $link;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getOriginalPriceVat()
    {
        return $this->originalPriceVat;
    }

    public function setOriginalPriceVat($originalPriceVat)
    {
        $this->originalPriceVat = $originalPriceVat;
        return $this;
    }

    public function getFinalPriceVat()
    {
        return $this->finalPriceVat;
    }

    public function setFinalPriceVat($finalPriceVat)
    {
        $this->finalPriceVat = $finalPriceVat;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getGtin()
    {
        return $this->gtin;
    }

    public function setGtin($gtin)
    {
        $this->gtin = $gtin;
        return $this;
    }

    public function getMpn()
    {
        return $this->mpn;
    }

    public function setMpn($mpn)
    {
        $this->mpn = $mpn;

        return $this;
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    public function getItemGroupId()
    {
        return $this->itemGroupId;
    }

    public function setItemGroupId($itemGroupId)
    {
        $this->itemGroupId = $itemGroupId;
        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    public function getAvailability()
    {
        return $this->availability;
    }

    public function setAvailability($availability)
    {
        $this->availability = $availability;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }
}
