<?php

namespace Andering\Generator\Generators\Nike;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;


class Inventory extends BaseItem {


    /** @var \DateTime|int @required */
    protected $date;

    /** @var string @required */
    protected $currency;

    /** @var string @required */
    protected $barcode;

    /** @var int @required */
    protected $quantity;

    /** @var float @required */
    protected $vat;

    /** @var float @required */
    protected $costPrice;



    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getBarcode()
    {
        return $this->barcode;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function setCostPrice($costPrice)
    {
        $this->costPrice = $costPrice;

        return $this;
    }

    public function getCostPrice()
    {
        return $this->costPrice;
    }

}
