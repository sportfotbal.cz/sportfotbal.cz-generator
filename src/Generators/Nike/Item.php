<?php

namespace Andering\Generator\Generators\Nike;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;


class Item extends BaseItem {

    /** @var Sale[] */
    protected $saleCollection = [];

    /** @var Inventory[] */
    protected $inventoryCollection = [];


    public function addSale($sale)
    {
        $this->saleCollection[] = $sale;

        return $this;
    }

    public function getSaleCollection()
    {
        return $this->saleCollection;
    }


    public function addInventory($inventory)
    {
        $this->inventoryCollection[] = $inventory;

        return $this;
    }

    public function getInventoryCollection()
    {
        return $this->inventoryCollection;
    }


}
