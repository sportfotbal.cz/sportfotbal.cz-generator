<?php

namespace Andering\Generator\Generators\Nike;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;


class Sale extends BaseItem {

    /** @var \DateTime|int @required */
    protected $saleDate;

    /** @var string @required */
    protected $type;

    /** @var string @required */
    protected $barcode;

/** @var string @required */
    protected $currency;

    /** @var int @required */
    protected $quantity;

    /** @var float @required */
    protected $vat;

    /** @var float @required */
    protected $priceWithoutDiscountIncVat;

    /** @var float @required */
    protected $priceWithDiscountIncVat;

    /** @var float @required */
    protected $discount;


    public function setSaleDate($saleDate)
    {
        $this->saleDate = $saleDate;

        return $this;
    }

    public function getSaleDate()
    {
        return $this->saleDate;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getBarcode()
    {
        return $this->barcode;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;

        return $this;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function setPriceWithoutDiscountIncVat($priceWithoutDiscountIncVat)
    {
        $this->priceWithoutDiscountIncVat = $priceWithoutDiscountIncVat;

        return $this;
    }

    public function getPriceWithoutDiscountIncVat()
    {
        return $this->priceWithoutDiscountIncVat;
    }

    public function setPriceWithDiscountIncVat($priceWithDiscountIncVat)
    {
        $this->priceWithDiscountIncVat = $priceWithDiscountIncVat;

        return $this;
    }

    public function getPriceWithDiscountIncVat()
    {
        return $this->priceWithDiscountIncVat;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

}
