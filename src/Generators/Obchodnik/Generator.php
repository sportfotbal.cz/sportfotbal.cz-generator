<?php
namespace Andering\Generator\Generators\Obchodnik;

use Andering\Generator\Generators\BaseGenerator;

 class Generator extends BaseGenerator {

    /**
     * @param $name
     * @return string
     */
    protected function getTemplate($name)
    {
        $reflection = new \ReflectionClass(__CLASS__);
        return dirname($reflection->getFileName()) . '/latte/' . $name . '.latte';
    }

}
