<?php

namespace Andering\Generator\Generators\Obchodnik;


use Andering, Nette;


class Image extends Nette\Object
{
    /** @var string */
    private $url;

    /**
     * Image constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = (string) $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }



}
