<?php

namespace Andering\Generator\Generators\Shopmania;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;

/**
 * Class Item
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Heureka
 * @see http://sluzby.heureka.cz/napoveda/xml-generator/ Documentation
 */
class Item extends BaseItem {


    /** @var string @required */
    protected $itemId;

    /** @var string @required */
    protected $mpc;

    /** @var string @required */
    protected $mpn;

    /** @var string|null */
    protected $manufacturer;

    /** @var string|null */
    protected $category;

    /** @var string|null */
    protected $name;

    /** @var string|null */
    protected $gtin;

    /** @var string @required */
    protected $product;

    /** @var string @required */
    protected $description;

    /**  @var string @required */
    protected $url;

    /** @var float @required */
    protected $price;

    /** @var string|null */
    protected $image;

    /** @var string|null */
    protected $currency;

    /** @var bool|null */
    protected $availability;



    /**
     * @return null|string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * @param null|string $itemId
     * @return Item
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getMpc()
    {
        return $this->mpc;
    }

    /**
     * @param null|string $mpc
     * @return Item
     */
    public function setMpc($mpc)
    {
        $this->mpc = $mpc;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getMpn()
    {
        return $this->mpn;
    }

    /**
     * @param null|string $mpn
     * @return Item
     */
    public function setMpn($mpn)
    {
        $this->mpn = $mpn;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getGtin()
    {
        return $this->gtin;
    }

    /**
     * @param null|string $mpn
     * @return Item
     */
    public function setGtin($gtin)
    {
        $this->gtin = $gtin;

        return $this;
    }


    /**
     * @return null|string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @param null|string $manufacturer
     * @return Item
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param null|string $categoryText
     * @return Item
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }


    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null|string
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string $product
     * @return Item
     */
    public function setProduct($product)
    {
        $this->product = (string)$product;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = (string)$description;

        return $this;
    }


    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Item
     */
    public function setUrl($url)
    {
        $this->url = (string)$url;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $priceVat
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = (float)$price;

        return $this;
    }



    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return $this
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }


    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Item
     */
    public function setCurrency($currency)
    {
        $this->currency = (string)$currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     * @return Item
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
        return $this;
    }

}
