<?php

namespace Andering\Generator\Generators;


use Andering, Nette;

/**
 * Class BaseItem
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Zbozi
 */
abstract class BaseItem extends Nette\Object implements Andering\Generator\Generators\IItem
{

	/**
	 * Validate item
	 * @return bool return true if item is valid
     */
	public function validate() {
		$reflection = $this->getReflection();

		foreach ($reflection->getProperties(\ReflectionProperty::IS_PUBLIC) as $v) {
			if ($v->getAnnotation('required')) {
				if (!isset($this->{$v->getName()})) {
					return FALSE;
				}
			}
		}

		return TRUE;
	}
}
