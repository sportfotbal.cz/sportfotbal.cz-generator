<?php

namespace Andering\Generator\Generators\Shoptet;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;

/**
 * Class Item
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Shoptet
 * @see http://sluzby.heureka.cz/napoveda/xml-generator/ Documentation
 */
class Item extends BaseItem {


    /** @var string @required */
    protected $id;

    /** @var string|null */
    protected $size;

    /** @var string|null */
    protected $quantity;


   /**
     * @return null|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     * @return Item
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     * @return Item
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getQuantity()
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     * @return Item
     */
    public function setQuantity($size)
    {
        $this->size = $size;

        return $this;
    }

}
