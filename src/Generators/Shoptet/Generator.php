<?php
namespace Andering\Generator\Generators\Shoptet;

use Andering\Generator\Generators\BaseGenerator;

/**
 * Class ShoptetGenerator
 */
class Generator extends BaseGenerator {

    protected function getTemplate($name)
    {
        $reflection = new \ReflectionClass(__CLASS__);
        return dirname($reflection->getFileName()) . '/latte/' . $name . '.latte';
    }

}
