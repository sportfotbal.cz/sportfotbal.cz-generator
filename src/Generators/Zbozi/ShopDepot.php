<?php

namespace Andering\Generator\Generators\Zbozi;

use Andering, Nette;

class ShopDepot extends Nette\Object {

    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

}
