<?php

namespace Andering\Generator\Generators\Zbozi;

use Andering, Nette;

class Image extends Nette\Object {
    private $url;

    /**
     * Image constructor.
     * @param $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

}
