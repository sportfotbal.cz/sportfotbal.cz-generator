<?php

namespace Andering\Generator\Generators\Zbozi;

use Andering;
use Nette;

/**
 * Class ExtraMessage
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Zbozi
 */
class CategoryText extends Nette\Object {


    /** @var string */
    protected $text;

    /**
     * ExtraMessage constructor.
     * @param $text
     */
    public function __construct($text)
    {

        $this->text = (string)$text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

}
