<?php
namespace Andering\Generator\Generators\Zbozi;

use Andering\Generator\Generators\BaseGenerator;

/**
 * Class ZboziGenerator
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators
 * @see http://napoveda.seznam.cz/cz/zbozi/specifikace-xml-pro-obchody/specifikace-xml-generatoru/ Documentation
 */
class Generator extends BaseGenerator {

    /**
     * @param $name
     * @return string
     */
    protected function getTemplate($name)
    {
        $reflection = new \ReflectionClass(__CLASS__);
        return dirname($reflection->getFileName()) . '/latte/' . $name . '.latte';
    }

}
