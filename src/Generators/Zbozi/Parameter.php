<?php

namespace Andering\Generator\Generators\Zbozi;

use Andering, Nette;

/**
 * Class Parameter
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Zbozi
 * @see http://napoveda.seznam.cz/cz/zbozi/specifikace-xml-pro-obchody/specifikace-xml-generatoru/#PARAM
 */
class Parameter extends Nette\Object {

    protected $name;
    protected $value;
    protected $unit;

    /**
     * Parameter constructor.
     * @param $name
     * @param $value
     * @param $unit
     */
    public function __construct($name, $value, $unit = null)
    {
        $this->name = (string)$name;
        $this->value = (string)$value;
        $this->unit = isset($unit) ? (string) $unit : null;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return null
     */
    public function getUnit()
    {
        return $this->unit;
    }

}
