<?php

namespace Andering\Generator\Generators\Google;

use Andering, Nette;
use Andering\Generator\Generators\BaseItem;

/**
 * Class Item
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators\Heureka
 * @see http://sluzby.heureka.cz/napoveda/xml-generator/ Documentation
 */
class Item extends BaseItem {

    /** @var string @required */
    protected $id;

    /** @var string @required */
    protected $title;

    /** @var string */
    protected $adult;

    /** @var string|null */
    protected $product;

    /** @var string @required */
    protected $description;

    /**  @var string @required */
    protected $link;

    /** @var Image[] */
    protected $images = [];

    /** @var string|null */
    protected $videoUrl;

    /** @var float @required */
    protected $price;

    /** @var string|null */
    protected $itemType;

    /** @var Parameter[] */
    protected $parameters = [];

    /** @var string|null */
    protected $manufacturer;

    /** @var string|null */
    protected $googleProductCategory;

    /** @var string|null */
    protected $gtin;

    /** @var string|null */
    protected $mpn;

    /** @var string|null */
    protected $color;

/** @var string|null */
    protected $size;


/** @var string|null */
    protected $sizeSystem;

    /** @var string|null */
    protected $currency;

    /** @var string|null */
    protected $isbn;

    /** @var float|null */
    protected $heurekaCpc;

    /** @var \DateTime|int @required */
    protected $deliveryDate;

    /** @var Delivery[] */
    protected $deliveries = [];

    /** @var string|null */
    protected $itemGroupId;

    /** @var string|null */
    protected $availability;

    /** @var string|null */
    protected $quantity;

    /** @var string|null */
    protected $shippingLabel;

    /** @var string|null */
    protected $condition;

    /** @var array */
    protected $accessories = [];

    /** @var float */
    protected $dues = 0;

    /** @var Gift[] */
    protected $gifts = [];

    /**
     * @return float
     */
    public function getDues()
    {
        return $this->dues;
    }

    /**
     * @param float $dues
     * @return Item
     */
    public function setDues($dues)
    {
        $this->dues = (float)$dues;

        return $this;
    }

    /**
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * @param string $videoUrl
     * @return $this
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * @param $url
     * @return $this
     */
    public function addImage($url)
    {
        $this->images[] = new Image($url);

        return $this;
    }

    /**
     * @param $id
     * @param $price
     * @param null $priceCod
     * @return $this
     */
    public function addDelivery($id, $price, $priceCod = null)
    {
        $this->deliveries[] = new Delivery($id, $price, $priceCod);

        return $this;
    }

    /**
     * @return Delivery[]
     */
    public function getDeliveries()
    {
        return $this->deliveries;
    }

    /**
     * @param $name
     * @param $val
     * @param null $unit
     * @return Item
     */
    public function addParameter($name, $val, $unit = null)
    {
        $this->parameters[] = new Parameter($name, $val, $unit);

        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    public function addGift($name)
    {
        $this->gifts[] = new Gift($name);

        return $this;
    }

    /**
     * @param $id
     * @return $this
     */
    public function addAccessory($id)
    {
        $this->accessories[] = $id;

        return $this;
    }

    /**
     * @return array
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Item
     */
    public function setTitle($title)
    {
        $this->title = (string)$title;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdult()
    {
        return $this->adult;
    }

    /**
     * @param string $title
     * @return Item
     */
    public function setAdult($adult)
    {
        $this->adult = (string)$adult;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = (string)$description;

        return $this;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return Item
     */
    public function setLink($link)
    {
        $this->link = (string)$link;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = (float)$price;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate instanceof \DateTime ? $this->deliveryDate->format('Y-m-d') : $this->deliveryDate;
    }

    /**
     * @param int|\DateTime $deliveryDate
     * @return Item
     */
    public function setDeliveryDate($deliveryDate)
    {
        if (!is_int($deliveryDate) && !($deliveryDate instanceof \DateTime)) {
            throw new \InvalidArgumentException("Delivery date must be integer or DateTime");
        }
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null|string $id
     * @return Item
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getGtin()
    {
        return $this->gtin;
    }

    /**
     * @param null|string $gtin
     * @return Item
     */
    public function setGtin($gtin)
    {
        $this->gtin = $gtin;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getMpn()
    {
        return $this->mpn;
    }

    /**
     * @param null|string $mpn
     * @return Item
     */
    public function setMpn($mpn)
    {
        $this->mpn = $mpn;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param null|string $color
     * @return Item
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param null|string $size
     * @return Item
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getSizeSystem()
    {
        return $this->sizeSystem;
    }

    /**
     * @param null|string $size
     * @return Item
     */
    public function setSizeSystem($sizeSystem)
    {
        $this->sizeSystem = $sizeSystem;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getIsbn()
    {
        return $this->isbn;
    }

    /**
     * @param null|string $isbn
     * @return Item
     */
    public function setIsbn($isbn)
    {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getItemGroupId()
    {
        return $this->itemGroupId;
    }

    /**
     * @param null|string $itemGroupId
     * @return Item
     */
    public function setItemGroupId($itemGroupId)
    {
        $this->itemGroupId = $itemGroupId;

        return $this;
    }


    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     * @return Item
     */
    public function setCurrency($currency)
    {
        $this->currency = (string)$currency;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param null|string $availability
     * @return Item
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param null|string $quantity
     * @return Item
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }


    /**
     * @return null|string
     */
    public function getShippingLabel()
    {
        return $this->shippingLabel;
    }

    /**
     * @param null|string $shippingLabel
     * @return Item
     */
    public function setShippingLabel($shippingLabel)
    {
        $this->shippingLabel = $shippingLabel;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param null|string $condition
     * @return Item
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param null|string $manufacturer
     * @return Item
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getGoogleProductCategory()
    {
        return $this->googleProductCategory;
    }

    /**
     * @param null|string $googleProductCategory
     * @return Item
     */
    public function setGoogleProductCategory($googleProductCategory)
    {
        $this->googleProductCategory = $googleProductCategory;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param null|string $product
     * @return Item
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * @return Image[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return Gift[]
     */
    public function getGifts()
    {
        return $this->gifts;
    }

    /**
     * @return Parameter[]
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @return null|string
     */
    public function getHeurekaCpc()
    {
        return $this->heurekaCpc;
    }

    /**
     * @param null|string $heurekaCpc
     * @return $this
     */
    public function setHeurekaCpc($heurekaCpc)
    {
        $this->heurekaCpc = $heurekaCpc;

        return $this;
    }

}
