<?php
namespace Andering\Generator\Generators\Google;

use Andering\Generator\Generators\BaseGenerator;

/**
 * Class HeurekaGenerator
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\Generators
 * @see http://sluzby.heureka.cz/napoveda/xml-generator/ Documentation
 */
class Generator extends BaseGenerator {

    protected function getTemplate($name)
    {
        $reflection = new \ReflectionClass(__CLASS__);
        return dirname($reflection->getFileName()) . '/latte/' . $name . '.latte';
    }

}
