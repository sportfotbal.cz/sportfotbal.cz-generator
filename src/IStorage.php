<?php
/**
 * Created by PhpStorm.
 * User: jam
 * Date: 27.1.15
 * Time: 19:30
 */

namespace Andering\Generator;


use Latte;
use Andering;

interface IStorage
{
	public function save($fileName, $content);
}
