<?php

namespace Andering\Generator\Command;

use Symfony\Component\Console\Command\Command,
	Symfony\Component\Console\Input\InputInterface,
	Symfony\Component\Console\Output\OutputInterface,
	Symfony\Component\Console\Input\InputOption;

use Nette,
	Andering;

class GeneratorCommand extends Command {

	/** @var \Nette\DI\Container */
	private $container;

	/** @var array */
	private $config;

	public function __construct(array $config = [], Nette\DI\Container $container)
	{
		parent::__construct();

		$this->container = $container;
		$this->config = $config;
	}

	protected function configure()
	{
		$this->setName('Generator:export')
			->setDescription('Export product generator')
			->addOption('show', 's', InputOption::VALUE_NONE, 'Print available exports')
			->addOption('generator', 'f', InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$show = $input->getOption('show');
		$generators = $input->getOption('generator');

		if ($show) {
			$output->writeln('Available exports:');

			foreach ($this->config['exports'] as $k => $v) {
				if ($v) {
					$output->writeln('- ' . $k);
				}
			}
		}

		$generators = $generators ?: array_keys($this->config['exports']);
		if (count($generators)) {
			foreach ($generators as $generator) {
				if (!isset($this->config['exports'][$generator]) || !$this->config['exports'][$generator]) {
					$output->writeln('Generator for ' . $generator . ' doesn\'t exist');
				}

				$generator = $this->container->getService('generator.' . $generator);

				$generator->save($generator . '.xml');
				$output->writeln('Generator ' . $generator . ' done');
			}
		}
	}
}
