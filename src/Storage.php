<?php

namespace Andering\Generator;

use Andering,
    Latte;

/**
 * Class Storage
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator
 */
class Storage implements IStorage {
    /** @var */
    private $dir;

    /**
     * Storage constructor.
     * @param $dir
     */
    function __construct($dir)
    {
        $this->dir = $dir;
    }

    /**
     * @param $filename
     * @param $content
     */
    public function save($filename, $content)
    {
        file_put_contents(realpath($this->dir) . DIRECTORY_SEPARATOR . $filename, $content);
    }

}
