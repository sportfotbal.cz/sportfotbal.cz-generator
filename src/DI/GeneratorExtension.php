<?php


namespace Andering\Generator\DI;

use Nette;

/**
 * Class GeneratorExtension
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator\DI
 */
class GeneratorExtension extends Nette\DI\CompilerExtension {
    /** @var array */
    private $defaults = [
        'exportsDir' => '%wwwDir%',
        'exports'    => []
    ];

    public function loadConfiguration()
    {
        parent::loadConfiguration();

        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        $builder->addDefinition($this->prefix('storage'))
            ->setClass('\Andering\Generator\Storage', [$config['exportsDir']]);

        foreach ($config['exports'] as $export => $class) {
            if (!class_exists($class)) {
            }
            $builder->addDefinition($this->prefix($export))
                ->setClass($class);

        }

        if (class_exists('\Symfony\Component\Console\Command\Command')) {
            $builder->addDefinition($this->prefix('command'))
                ->setClass('Andering\Generator\Command\GeneratorCommand', [$config])
                ->addTag('kdyby.console.command');
        }
    }
}
