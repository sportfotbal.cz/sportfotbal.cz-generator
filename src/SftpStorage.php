<?php

namespace Andering\Generator;

use Andering,
    Latte;
use phpseclib\Net;

/**
 * Class Storage
 * @author Martin Knor <martin.knor@gmail.com>
 * @package Andering\Generator
 */
class SftpStorage implements IStorage {

    private $connection;

    private $username;

    private $password;

    private $host;

    /**
     * Storage constructor.
     * @param $dir
     */
    public function __construct($username,$password,$host)
    {
        $this->username = $username;
        $this->password = $password;
        $this->host = $host;

    }


    public function getConnection()
    {
        if(!$this->connection){
            $connection = new Net\SFTP($this->host);
            $connection->login($this->username,$this->password);
            $this->connection = $connection;
        }

        return $this->connection;
    }


    public function save($filename, $content)
    {
        return $this->getConnection()->put($filename, $content, Net\SFTP::SOURCE_STRING);
    }

}
